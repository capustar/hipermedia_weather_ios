//
//  ViewController.swift
//  weather
//
//  Created by Antonio Pertusa on 18/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, XMLParserDelegate, ConnectionDelegate {
    
    @IBOutlet weak var cityName: UITextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    // Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cityName.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.cityName.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func floatToString(_ value: Float?) -> String
    {
        if let value = value {
            return "\(value)"
        }
        return "--"
    }
    
    func descargarImagen(urlIcon : String)
    {
        let urlString = "http://openweathermap.org/img/w/" + urlIcon + ".png"
        if let url = URL(string:urlString)
        {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            session.dataTask( with:url, completionHandler: { data, response, error in
                if error == nil, let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    DispatchQueue.main.async {
                        self.icon?.image = UIImage(data: data)
                        print ("Downloaded \(urlString)")
                    }
                }
            }).resume()
        }
    }
    
    func updateView(weather : Weather)
    {
        self.humidity.text = self.floatToString(weather.humidity)
        self.temperature.text = self.floatToString(weather.temperature)
        self.windSpeed.text = self.floatToString(weather.windSpeed)
        self.weatherDescription.text = weather.description
        self.country.text = weather.country
        
        // Descargar y actualizar la imagen aquí
        descargarImagen(urlIcon : weather.imageURL!)
    }


    func parseJSON(_ data: Data)
    {
        // Guardar data como un Dictionary
        // Mostrarlo por consola con print. () significa array, {} es diccionario
        // Llamar al constructor de weather para extraer la información
        // Llamar a updateView para actualizar las etiquetas correspondientes
        
        if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
        {
            let weather = Weather(json:jsonData)
            
            updateView(weather: weather!)
        }
        
    }
    
    func parseXML(_ data: Data)
    {
        // Inicializar el parser. La extracción se hará en los métodos delegados de XMLParserDelegate
        // En este caso, se creará un objeto de clase Weather y se escribirá en sus variables públicas conforme se va leyendo el XML, como puede verse abajo.
        let parser = XMLParser(data:data)
        parser.delegate = self
        let result = parser.parse()
    }
    
    func iniciarConexion( url: String)
    {
        if let encodedString = url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        {
            let encodedUrl = URL(string: encodedString)!
            let request = URLRequest(url: encodedUrl)
            let session = URLSession.shared
            
            session.dataTask(with: request, completionHandler:
                { data, response, error in
                    
                    // La respuesta del servidor se recibe en este punto.
                    // Se guardan los datos recibidos (data), la respuesta (response) y si ha habido algún error
                    
                    if let error = error
                    {
                        print(error.localizedDescription)
                    }
                    else
                    {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 200
                        {
                            DispatchQueue.main.async
                            {
                                    // Esperamos a que terminen de recibirse todos los datos
                                    // Guardamos los datos en un string con formato ASCII o UTF8
                                    let contents = String(data: data!, encoding: .ascii)!
                                    print (contents)
                                
                                let type = self.segmentedControl.titleForSegment(at: self.segmentedControl.selectedSegmentIndex)
                                
                                if type == "JSON"
                                {
                                    self.parseJSON(data!)
                                }
                                else if type == "XML"
                                {
                                    self.parseXML(data!)
                                }
                                else
                                {
                                    
                                }
                            }
                        }
                        else
                        {
                            print("Received status code: \(res.statusCode)")
                        }
                    }
            }).resume() // Con esta instrucción lanzamos la petición asíncrona
        }
    }
    
    
    @IBAction func search(_ sender: Any)
    {
        DispatchQueue.main.async
        {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        let type = self.segmentedControl.titleForSegment(at: self.segmentedControl.selectedSegmentIndex)
        
        let apiKey = "ba01089290fe01d324a7bd5dcfb5a603"
        let urlString : String?
        
        if type == "JSON"
        {
            urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(self.cityName.text!)&APPID=\(apiKey)"
        }
        else
        {
            urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(self.cityName.text!)&mode=xml&APPID=\(apiKey)"
        }

        // Se debe inicializar la conexión aquí
        iniciarConexion(url: urlString!)
    }
    
    func connectionSucceed(_ connection: Connection, with data: Data) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
        // Para debugging
        if let contents = String(data: data, encoding : .utf8) {
            print ("Received: \(contents)")
        }
        
        if self.segmentedControl.titleForSegment(at: self.segmentedControl.selectedSegmentIndex) == "XML" {
            self.parseXML(data)
        }
        else {
            self.parseJSON(data)
        }
        
        // URL de ejemplo para un icono: http://openweathermap.org/img/w/10d.png
    }
    
    func connectionFailed(_ connection: Connection, with error: String)
    {
        DispatchQueue.main.async
        {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        print(error)
    }
    
    // XML Parsing
    private var currentMessage : String?
    private var weather = Weather()!
    
    func parserDidStartDocument(_ parser: XMLParser)
    {
        self.weather = Weather()! // Creamos un objeto weather con valores por defecto
    }
    
    func parserDidEndDocument(_ parser: XMLParser)
    {
        self.updateView(weather: self.weather) // Actualizamos la vista cuando el parser haya terminado
    }

    // Falta añadir aquí los métodos delegados didStartElement, didEndElement y foundCharacters
    func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName: String?,
                attributes attributeDict: [String : String] = [:])
    {
        print("Entra")
        if elementName.lowercased() == "current"
        {
            // Ok, no hacer nada
        }
        else if elementName.lowercased() == "temperature"
        {
            self.weather.temperature = (attributeDict["value"] as! NSString).floatValue
        }
        else if elementName.lowercased() == "humidity"
        {
            self.weather.humidity = (attributeDict["value"] as! NSString).floatValue
        }
        else if elementName.lowercased() == "speed"
        {
            self.weather.windSpeed = (attributeDict["value"] as! NSString).floatValue
        }
        else if elementName.lowercased() == "weather"
        {
            self.weather.description = attributeDict["value"]
            self.weather.imageURL = attributeDict["icon"]
        }
    }
    
    func parser(_ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName: String?)
    {
        
    }
    
    func parser(_ parser: XMLParser,
                foundCharacters characters: String)
    {
        // Quitamos espacios en blanco
        let trimmedString = characters.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if trimmedString != ""
        {
            //self.currentMessage?.text = trimmedString
            self.weather.country = trimmedString
        }
    }
}

