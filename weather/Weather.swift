//
//  Weather.swift
//  weather
//
//  Created by Antonio Pertusa on 19/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import Foundation

class Weather {
    var humidity : Float? //
    var temperature : Float? //
    var windSpeed : Float? //
    var description : String? //
    var country : String? //
    var imageURL : String?
    
    init?(json: [String: Any])
    {
        guard
            let weatherJSON = json["weather"] as? [Any],
                let object = weatherJSON[0] as? [String : Any],
                let description = object["description"],
                let image_url = object["icon"],
            let mainJSON = json["main"] as? [String : Any],
                let humidity = mainJSON["humidity"],
                let temperature = mainJSON["temp"],
            let windJSON = json["wind"] as? [String : Any],
                let windSpeed = windJSON["speed"],
            let sysJSON = json["sys"] as? [String : Any],
                let country = sysJSON["country"]
        else
        {
                return nil
        }
        
        self.humidity = humidity as? Float
        self.temperature = temperature as? Float
        self.windSpeed = windSpeed as? Float
        self.description = description as? String
        self.country = country as? String
        self.imageURL = image_url as? String
    }
    
    init?() {
        // No hacer nada
    }
}
